--usp_Mascota_Insertar 'juanito', '2020-06-05', 'azul', 'perro', 'criolla','M','0', null
-- usp_Mascota_Actualizar 1, 'juanito1', '2020-06-05', 'azul', 'perro', 'criolla','M','0', null
-- usp_Mascota_Eliminar 2
-- usp_Mascota_Obtener 1
-- usp_Mascota_Listar

use Mascotas
GO

CREATE PROCEDURE usp_Mascota_Insertar(
 @Nombre VARCHAR(400),
 @Nacimiento DATE,
 @Color varchar(100),
 @Especie varchar(200),
 @Raza varchar(200),
 @Sexo char(1),
 @EstaAdoptado bit,
 @Foto varchar(1000)
)
AS

  INSERT INTO Mascota(
	 Nombre,
	 Nacimiento,
	 Color,
	 Especie,
	 Raza,
	 Sexo,
	 EstaAdoptado,
	 Foto  

  )
  VALUES
  (
	 @Nombre,
	 @Nacimiento,
	 @Color,
	 @Especie,
	 @Raza,
	 @Sexo,
	 @EstaAdoptado,
	 @Foto
  );

SELECT SCOPE_IDENTITY() as Id

GO


CREATE PROCEDURE usp_Mascota_Actualizar(
 @MascotaId INT,
 @Nombre VARCHAR(400),
 @Nacimiento DATE,
 @Color varchar(100),
 @Especie varchar(200),
 @Raza varchar(200),
 @Sexo char(1),
 @EstaAdoptado bit,
 @Foto varchar(1000)
)
AS

  UPDATE Mascota SET 
	 Nombre = @Nombre,
	 Nacimiento = @Nacimiento,
	 Color = @Color,
	 Especie = @Especie,
	 Raza = @Raza,
	 Sexo = @Sexo,
	 EstaAdoptado = @EstaAdoptado,
	 Foto = @Foto
 WHERE
	MascotaId = @MascotaId;	


GO


CREATE PROCEDURE usp_Mascota_Eliminar(
 @MascotaId INT
)
AS

  DELETE FROM Mascota
 WHERE
	MascotaId = @MascotaId;	


GO

CREATE PROCEDURE usp_Mascota_Obtener(
 @MascotaId INT
)
AS

 SELECT
		 MascotaId,
		 Nombre,
		 Nacimiento,
		 Color,
		 Especie,
		 Raza,
		 Sexo,
		 EstaAdoptado,
		 Foto 
 FROM Mascota
 WHERE
	MascotaId = @MascotaId;	


GO


CREATE PROCEDURE usp_Mascota_Listar
AS

 SELECT
		 MascotaId,
		 Nombre,
		 Nacimiento,
		 Color,
		 Especie,
		 Raza,
		 Sexo,
		 EstaAdoptado,
		 Foto 
 FROM Mascota;


GO