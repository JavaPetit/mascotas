use Mascotas
GO

CREATE PROCEDURE usp_SolicitudAdopcion_Insertar(
 @Motivo varchar(400),
 @Fecha date,
 @Direccion varchar(400),
 @Nombres varchar(400),
 @ApellidoPaterno varchar(400) ,
 @ApellidoMaterno varchar(400) ,
 @Estado varchar(100),
 @MascotaId INT
)
AS

  INSERT INTO SolicitudAdopcion(
				 Motivo,
				 Fecha ,
				 Direccion ,
				 Nombres ,
				 ApellidoPaterno  ,
				 ApellidoMaterno ,
				 Estado,
				 MascotaId

  )
  VALUES
  (
				 @Motivo,
				 @Fecha ,
				 @Direccion ,
				 @Nombres ,
				 @ApellidoPaterno  ,
				 @ApellidoMaterno ,
				 @Estado,
				 @MascotaId
  );

SELECT SCOPE_IDENTITY() as Id

GO


CREATE PROCEDURE usp_SolicitudAdopcion_Actualizar(
 @SolicitudAdopcionId int,
 @Motivo varchar(400),
 @Fecha date,
 @Direccion varchar(400),
 @Nombres varchar(400),
 @ApellidoPaterno varchar(400) ,
 @ApellidoMaterno varchar(400) ,
 @Estado varchar(100),
 @MascotaId INT
)
AS

  UPDATE SolicitudAdopcion SET 
			 Motivo = @Motivo,
			 Fecha = @Fecha,
			 Direccion = @Direccion,
			 Nombres = @Nombres,
			 ApellidoPaterno = @ApellidoPaterno ,
			 ApellidoMaterno = @ApellidoMaterno ,
			 Estado = @Estado,
			 MascotaId = @MascotaId
 WHERE
	SolicitudAdopcionId = @SolicitudAdopcionId;	


GO


CREATE PROCEDURE usp_SolicitudAdopcion_Eliminar(
 @SolicitudAdopcionId INT
)
AS

  DELETE FROM SolicitudAdopcion
 WHERE
	SolicitudAdopcionId = @SolicitudAdopcionId;	


GO

CREATE PROCEDURE usp_SolicitudAdopcion_Obtener(
 @SolicitudAdopcionId INT
)
AS

 SELECT
		 SolicitudAdopcionId,
		 Motivo,
		 Fecha,
		 Direccion ,
		 Nombres ,
		 ApellidoPaterno ,
		 ApellidoMaterno ,
		 Estado ,
		 MascotaId
 FROM SolicitudAdopcion
 WHERE
	SolicitudAdopcionId = @SolicitudAdopcionId;	


GO


CREATE PROCEDURE usp_SolicitudAdopcion_Listar
AS

 SELECT
		 SolicitudAdopcionId,
		 Motivo,
		 Fecha,
		 Direccion ,
		 Nombres ,
		 ApellidoPaterno ,
		 ApellidoMaterno ,
		 Estado ,
		 MascotaId
 FROM SolicitudAdopcion;


GO