create database Mascotas
GO

use Mascotas
GO

CREATE TABLE Mascota(
 MascotaId int primary key identity(1,1) not null,
 Nombre varchar(400) not null,
 Nacimiento date null,
 Color varchar(100),
 Especie varchar(200),
 Raza varchar(200),
 Sexo char(1),
 EstaAdoptado bit not null default 0,
 Foto varchar(1000) 
);

GO

CREATE TABLE VisitaMedica(
 VisitaMedicaId int primary key identity(1,1) not null,
 Motivo varchar(400) not null,
 Medico varchar(200) not null,
 Fecha date not null,
 CondicionCorporal varchar(400),
 Diagnostico varchar(600),
 Tratamiento varchar(600),
 MascotaId INT  FOREIGN KEY REFERENCES  Mascota(MascotaId)
);

GO

CREATE TABLE VisitaHogar(
 VisitaHogarId int primary key identity(1,1) not null,
 Motivo varchar(400) not null,
 Fecha date not null,
 Direccion varchar(400),
 Observacion varchar(1000),
 MascotaId INT  FOREIGN KEY REFERENCES  Mascota(MascotaId)
);

GO

CREATE TABLE SolicitudAdopcion(
 SolicitudAdopcionId int primary key identity(1,1) not null,
 Motivo varchar(400) not null,
 Fecha date not null,
 Direccion varchar(400),
 Nombres varchar(400) not null,
 ApellidoPaterno varchar(400) not null,
 ApellidoMaterno varchar(400) not null,
 Estado varchar(100) not null,
 MascotaId INT  FOREIGN KEY REFERENCES  Mascota(MascotaId)
);

GO


CREATE TABLE Usuario(
	[Username] varchar(200) primary key,
	[Password] varchar(200) not null
)

GO
