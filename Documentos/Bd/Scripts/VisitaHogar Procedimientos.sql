use Mascotas
GO
CREATE PROCEDURE usp_VisitaHogar_Insertar(
 @Motivo varchar(400),
 @Fecha date,
 @Direccion varchar(400),
 @Observacion varchar(1000),
 @MascotaId INT
)
AS

  INSERT INTO VisitaHogar(
		  Motivo,
		  Fecha,
		  Direccion,
		  Observacion,
		  MascotaId

  )
  VALUES
  (
		  @Motivo,
		  @Fecha,
		  @Direccion,
		  @Observacion,
		  @MascotaId
  );

SELECT SCOPE_IDENTITY() as Id

GO


CREATE PROCEDURE usp_VisitaHogar_Actualizar(
 @VisitaHogarId int,
 @Motivo varchar(400),
 @Fecha date,
 @Direccion varchar(400),
 @Observacion varchar(1000),
 @MascotaId INT
)
AS

  UPDATE VisitaHogar SET 
		  Motivo = @Motivo,
		  Fecha = @Fecha,
		  Direccion = @Direccion,
		  Observacion = @Observacion,
		  MascotaId = @MascotaId
 WHERE
	VisitaHogarId = @VisitaHogarId;	


GO


CREATE PROCEDURE usp_VisitaHogar_Eliminar(
 @VisitaHogarId INT
)
AS

  DELETE FROM VisitaHogar
 WHERE
	VisitaHogarId = @VisitaHogarId;	


GO

CREATE PROCEDURE usp_VisitaHogar_Obtener(
 @VisitaHogarId INT
)
AS

 SELECT
		 VisitaHogarId,
		 Motivo,
		 Fecha,
		 Direccion,
		 Observacion,
		 MascotaId
 FROM VisitaHogar
 WHERE
	VisitaHogarId = @VisitaHogarId;	


GO


CREATE PROCEDURE usp_VisitaHogar_Listar
AS

 SELECT
		 VisitaHogarId,
		 Motivo,
		 Fecha,
		 Direccion,
		 Observacion,
		 MascotaId
 FROM VisitaHogar;


GO