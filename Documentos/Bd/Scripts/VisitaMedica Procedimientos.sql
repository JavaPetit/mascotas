use Mascotas
GO
CREATE PROCEDURE usp_VisitaMedica_Insertar(
 @Motivo varchar(400),
 @Medico varchar(200),
 @Fecha date,
 @CondicionCorporal varchar(400),
 @Diagnostico varchar(600),
 @Tratamiento varchar(600),
 @MascotaId INT
)
AS

  INSERT INTO VisitaMedica(
		 Motivo,
		 Medico,
		 Fecha,
		 CondicionCorporal,
		 Diagnostico,
		 Tratamiento,
		 MascotaId 

  )
  VALUES
  (
	 @Motivo,
	 @Medico,
	 @Fecha,
	 @CondicionCorporal,
	 @Diagnostico,
	 @Tratamiento,
	 @MascotaId 
  );

SELECT SCOPE_IDENTITY() as Id

GO


CREATE PROCEDURE usp_VisitaMedica_Actualizar(
 @VisitaMedicaId INT,
 @Motivo varchar(400),
 @Medico varchar(200),
 @Fecha date,
 @CondicionCorporal varchar(400),
 @Diagnostico varchar(600),
 @Tratamiento varchar(600),
 @MascotaId INT
)
AS

  UPDATE VisitaMedica SET 
		 Motivo = @Motivo,
		 Medico = @Medico,
		 Fecha = @Fecha,
		 CondicionCorporal = @CondicionCorporal,
		 Diagnostico = @Diagnostico,
		 Tratamiento = @Tratamiento,
		 MascotaId = @MascotaId
 WHERE
	VisitaMedicaId = @VisitaMedicaId;	


GO


CREATE PROCEDURE usp_VisitaMedica_Eliminar(
 @VisitaMedicaId INT
)
AS

  DELETE FROM VisitaMedica
 WHERE
	VisitaMedicaId = @VisitaMedicaId;	


GO

CREATE PROCEDURE usp_VisitaMedica_Obtener(
 @VisitaMedicaId INT
)
AS

 SELECT
		 VisitaMedicaId,
		 Motivo,
		 Medico,
		 Fecha,
		 CondicionCorporal,
		 Diagnostico,
		 Tratamiento,
		 MascotaId
 FROM VisitaMedica
 WHERE
	VisitaMedicaId = @VisitaMedicaId;	


GO


CREATE PROCEDURE usp_VisitaMedica_Listar
AS

 SELECT
		 VisitaMedicaId,
		 Motivo,
		 Medico,
		 Fecha,
		 CondicionCorporal,
		 Diagnostico,
		 Tratamiento,
		 MascotaId
 FROM VisitaMedica;


GO