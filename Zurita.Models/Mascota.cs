﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zurita.Models
{
    public class Mascota
    {

        public int Id { get; set; }

        public int MascotaId { get; set; }

        public string Nombre { get; set; }

        public DateTime? Nacimiento { get; set; }

        public string Color { get; set; }

        public string Especie { get; set; }

        public string Raza { get; set; }

        public string Sexo { get; set; }

        public bool EstaAdoptado { get; set; }

        public string Foto { get; set; }

        public string UrlVideoYoutube { get; set; }

        public string Descripcion { get; set; }
    }
}
