﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zurita.Models
{
    public class SolicitudAdopcion
    {

        public int Id { get; set; }

        public int SolicitudAdopcionId { get; set; }

        public string Motivo { get; set; }

        public DateTime Fecha { get; set; }

        public string Direccion { get; set; }

        public string Nombres { get; set; }

        public string ApellidoPaterno { get; set; }

        public string ApellidoMaterno { get; set; }

        public string Estado { get; set; }

        public int? MascotaId { get; set; }

        public string Username { get; set; }

        public string Mascota { get; set; }
    }
}
