﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zurita.Models
{
    public class Usuario
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string Correo { get; set; }

        public string Nombres { get; set; }

        public string Apellidos { get; set; }

        public string Direccion { get; set; }

        public string Telefono { get; set; }

        public bool EsAdministrador { get; set; }

    }
}
