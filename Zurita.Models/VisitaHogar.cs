﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zurita.Models
{
    public class VisitaHogar
    {

        public int Id { get; set; }

        public int VisitaHogarId { get; set; }

        public string Motivo { get; set; }

        public DateTime Fecha { get; set; }

        public string Direccion { get; set; }

        public string Observacion { get; set; }

        public int? MascotaId { get; set; }
    }
}
