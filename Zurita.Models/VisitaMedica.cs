﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zurita.Models
{
    public class VisitaMedica
    {

        public int Id { get; set; }

        public int VisitaMedicaId { get; set; }

        public string Motivo { get; set; }

        public string Medico { get; set; }

        public DateTime Fecha { get; set; }

        public string CondicionCorporal { get; set; }

        public string Diagnostico { get; set; }

        public string Tratamiento { get; set; }

        public int? MascotaId { get; set; }
    }
}
