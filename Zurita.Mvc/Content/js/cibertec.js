﻿(function (cibertec) {

    cibertec.getModal = getModalContent;
    cibertec.closeModal = closeModal;
    return cibertec;

    function getModalContent(url) {
        $.get(url, function (data) {
            $('.modal-body').html(data);
        });
    }

    function closeModal(option) {
        $("button[data-dismiss='modal']").click();
        $('.modal-body').html("");
        console.log("Close Modal");
        modifyAlertsClassess(option);
    }


    function modifyAlertsClassess(option) {
        $('#createMessage').hide();
        $('#editMessage').hide();
        $('#deleteMessage').hide();
        var idOcultar = "";

        if (option === 'create') {
            $('#createMessage').show('hidden');
            idOcultar = '#createMessage';

            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Creación exitosa!',
                showConfirmButton: false,
                timer: 1500
            });
        }
        else if (option === 'edit') {
            $('#editMessage').show('hidden');
            idOcultar = '#editMessage';

            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Edición exitosa!',
                showConfirmButton: false,
                timer: 1500
            });
        }
        else if (option === 'delete') {
            $('#deleteMessage').show('hidden');
            idOcultar = '#deleteMessage';

            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Eliminación exitosa!',
                showConfirmButton: false,
                timer: 1500
            });

        }

        window.setTimeout(function () {
            $(idOcultar).fadeTo(500, 0).slideUp(1000, function () {
                $(this).css('opacity', '1').hide();
            });
        }, 3000);

    }

})(window.cibertec = window.cibertec || {});