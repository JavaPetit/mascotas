﻿(function (detallePublico) {


    detallePublico.iniciarAdopcion = iniciarAdopcion;
    detallePublico.cancelarAdopcion = cancelarAdopcion;



    function iniciarAdopcion() {

        var url = $('#__URL_INICIAR_ADOPCION').val();
        $.ajax(
            {
                url: url,
                method: 'POST',
                data: {
                    "IdMascota": $('#__ID').val()
                },
                success: function (data) {
                    $('#__ID_SOLICITUD').val(data.id);
                    $('#btnSolicitar').hide();
                    $('#btnCancelar').show();
                    alert("Solicitud creada exitosamente");
                }
            }
        );
    }

    function cancelarAdopcion() {

        var url = $('#__URL_CANCELAR_ADOPCION').val();
        $.ajax(
            {
                url: url,
                method: 'POST',
                data: {
                    "IdSolicitudAdopcion": $('#__ID_SOLICITUD').val()
                },
                success: function (data) {
                    $('#btnSolicitar').show();
                    $('#btnCancelar').hide();

                    $('#__ID_SOLICITUD').val(0);
                    alert("Solicitud cancelada exitosamente");
                }
            }
        );
    }



})(window.detallePublico = window.detallePublico || {});
