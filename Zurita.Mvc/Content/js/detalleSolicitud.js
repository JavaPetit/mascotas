﻿(function (detalleSolicitud) {


    detalleSolicitud.cancelarAdopcion = cancelarAdopcion;


    function cancelarAdopcion() {

        var url = $('#__URL_CANCELAR_ADOPCION').val();
        $.ajax(
            {
                url: url,
                method: 'POST',
                data: {
                    "IdSolicitudAdopcion": $('#__ID').val()
                },
                success: function (data) {
                    alert("Solicitud cancelada exitosamente");
                    location.href = $('#__URL_SOLICITUDES').val();
                }
            }
        );
    }



})(window.detalleSolicitud = window.detalleSolicitud || {});
