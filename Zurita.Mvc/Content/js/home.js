﻿(function (home) {


    home.cargarDatos = cargarDatos;
    home.limpiar = limpiar;

    $(function () {
        cargarDatos();
    });

    function cargarDatos() {
        var url = $('#__URL_LISTA').val();
        $.ajax(
            {
                url: url,
                method: 'GET',
                data: {
                    "especie": $('#ddlEspecies').val(),
                    "sexo": $('#ddlSexo').val()
                },
                success: function (data) {
                    $('#blog').html(data);
                }
            }
        );
    }

    function limpiar() {
        $('#ddlEspecies').val('');
        $('#ddlSexo').val('');
        cargarDatos();
    }



})(window.home = window.home || {});
