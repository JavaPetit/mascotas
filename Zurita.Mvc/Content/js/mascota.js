﻿(function (mascota) {


    mascota.success = successReload;

    $(function () {
        cargarDatos();
    });

    function cargarDatos() {
        var url = $('#__URL_LISTA').val();
        $.get(url, function (data) {
            $('.contenido').html(data);
        });
    }

    function successReload(option) {
        console.log("Success Reload");
        cibertec.closeModal(option);
        cargarDatos();
        //Uso del DOM: Document Object Model
    }

})(window.mascota = window.mascota || {});
