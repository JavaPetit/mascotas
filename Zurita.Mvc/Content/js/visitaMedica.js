﻿(function (visitaMedica) {

    visitaMedica.hub = {};
    visitaMedica.ids = [];
    visitaMedica.recordInUse = false;

    visitaMedica.addVisita = addVisitaId;
    visitaMedica.removeVisita = removeVisitaId;
    visitaMedica.validate = validate;


    visitaMedica.success = successReload;

    $(function () {
        connectToHub();
        cargarDatos();
    });

    function cargarDatos() {
        var url = $('#__URL_LISTA').val();
        $.get(url, function (data) {
            $('.contenido').html(data);
        });
    }

    function successReload(option) {
        console.log("Success Reload");
        cibertec.closeModal(option);
        cargarDatos();
        //Uso del DOM: Document Object Model
    }

    function addVisitaId(id) {
        visitaMedica.hub.server.addVisitaMedicaId(id);
    }

    function removeVisitaId(id) {
        visitaMedica.hub.server.removeVisitaMedicaId(id);
    }

    function connectToHub() {
        visitaMedica.hub = $.connection.visitaMedicaHub;
        visitaMedica.hub.client.visitaMedicaStatus = visitaMedicaStatus;
    }

    function visitaMedicaStatus(visitaIds) {
        console.log(visitaIds);
        visitaMedica.ids = visitaIds;
    }

    function validate(id) {
        console.log("Id para validar " + id);
        visitaMedica.recordInUse = visitaMedica.ids.includes(id);

        console.log("Record utilizado: " + visitaMedica.recordInUse);

        if (visitaMedica.recordInUse) {
            $('#inUse').removeClass('hidden');
            $('#btn-save').html("");
        }
    }


})(window.visitaMedica = window.visitaMedica || {});