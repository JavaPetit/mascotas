﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Zurita.Models;
using Zurita.Mvc.ViewModels.Account;
using Zurita.UnitOfWork;

namespace Zurita.Mvc.Controllers
{
    public class AccountController : BaseController
    {

        public AccountController(ILog log, IUnitOfWork unit) : base(log, unit)
        {
        }

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        [AllowAnonymous]
        public PartialViewResult MenuInfo()
        {

            if (!Request.IsAuthenticated)
            {
                return PartialView("MenuInfo", new Usuario());
            }

            var username = default(string);
            var claims = ((ClaimsIdentity)User.Identity).Claims;
            var claim = claims.SingleOrDefault(m => m.Type == ClaimTypes.NameIdentifier);

            if (claim != null)
            {
                username = claim.Value;
            }

            var usuario = _unit.Usuarios.BuscarPorUsuario(username);
            if (usuario == null)
            {
                usuario = new Usuario();
            }


            return PartialView("MenuInfo", usuario);
        }


        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [AllowAnonymous]
        public ActionResult Registro()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel user)
        {
            if (!ModelState.IsValid) return View(user);
            var validUser = _unit.Usuarios.BuscarPorUsuarioYPassword(user.Email, user.Password);
            if (validUser == null)
            {
                ModelState.AddModelError(string.Empty, "Invalid email or password");
                return View(user);
            }

            var identity = new ClaimsIdentity(new[]{
                new Claim(ClaimTypes.Email, validUser.Correo),
                //new Claim(ClaimTypes.Role, validUser.Roles),
                new Claim(ClaimTypes.Name, $"{validUser.Nombres} {validUser.Apellidos}"),
                new Claim(ClaimTypes.NameIdentifier, validUser.Username)
            }, "ApplicationCookie");

            var context = Request.GetOwinContext();
            var authManager = context.Authentication;

            authManager.SignIn(identity);

            return RedirectToLocal(user.ReturnUrl);
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult Registro(RegistrarUsuarioViewModel userView)
        {
            if (!ModelState.IsValid) return View(userView);

            var user = new Usuario
            {
                Username = userView.Username,
                Correo = userView.Email,
                Nombres = userView.Nombres,
                Apellidos = userView.Apellidos,
                Direccion = userView.Direccion,
                Password = userView.Password,
                Telefono = userView.Telefono
            };

            _unit.Usuarios.Insert(user);

            var validUser = _unit.Usuarios.BuscarPorUsuarioYPassword(userView.Username, userView.Password);

            if (validUser == null)
            {
                ModelState.AddModelError(string.Empty, "Invalid email or password");
                return View(userView); //cambiar variable user por userView
            }

            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public ActionResult MiUsuario()
        {
            var username = default(string);
            var claims = ((ClaimsIdentity)User.Identity).Claims;
            var claim = claims.SingleOrDefault(m => m.Type == ClaimTypes.NameIdentifier);

            if (claim != null)
            {
                username = claim.Value;
            }

            var usuario = _unit.Usuarios.BuscarPorUsuario(username);

            var miUsuarioViewModel = new MiUsuarioViewModel()
            {
                Apellidos = usuario.Apellidos,
                Direccion = usuario.Direccion,
                Email = usuario.Correo,
                Nombres = usuario.Nombres,
                Telefono = usuario.Telefono,
                Username = usuario.Username
            };


            return View(miUsuarioViewModel);
        }

        [HttpPost]
        public ActionResult MiUsuario(MiUsuarioViewModel userView)
        {
            if (!ModelState.IsValid) return View(userView);

            var username = default(string);
            var claims = ((ClaimsIdentity)User.Identity).Claims;
            var claim = claims.SingleOrDefault(m => m.Type == ClaimTypes.NameIdentifier);

            if (claim != null)
            {
                username = claim.Value;
            }
            var entidad = _unit.Usuarios.BuscarPorUsuario(username);

            entidad.Apellidos = userView.Apellidos;
            entidad.Direccion = userView.Direccion;
            entidad.Nombres = userView.Nombres;
            entidad.Telefono = userView.Telefono;
            entidad.Correo = userView.Email;

            if (!string.IsNullOrWhiteSpace(userView.Password))
            {
                entidad.Password = userView.Password;
            }

            _unit.Usuarios.Update(entidad);

            return View();
        }


        public ActionResult Logout()
        {
            var context = Request.GetOwinContext();
            var authManager = context.Authentication;
            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("Login", "Account");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}