﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Zurita.Models;
using Zurita.Mvc.ViewModels.Mascota;
using Zurita.Mvc.ViewModels.SolicitudAdopcion;
using Zurita.UnitOfWork;

namespace Zurita.Mvc.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        protected readonly IUnitOfWork _unit;
        protected readonly ILog _log;

        public BaseController(ILog log, IUnitOfWork unit)
        {
            _log = log;
            _unit = unit;
        }

        protected List<SelectListItem> ObtenerMascotasSelect()
        {
            var lista = _unit.Mascotas.GetList().Select(e => new SelectListItem()
            {
                Text = e.Nombre,
                Value = e.MascotaId.ToString()
            }).ToList();

            lista.Insert(0, new SelectListItem()
            {
                Text = "Seleccionar Mascota",
                Value = "0"
            });

            return lista;

        }

        protected List<SelectListItem> ObtenerUsuariosSelect()
        {
            var lista = _unit.Usuarios.GetList().Select(e => new SelectListItem()
            {
                Text = e.Username,
                Value = e.Username
            }).ToList();

            lista.Insert(0, new SelectListItem()
            {
                Text = "Seleccionar Usuario",
                Value = ""
            });

            return lista;

        }

        protected List<SelectListItem> ObtenerEstadosSolicitudSelect()
        {
            var lista = new List<SelectListItem>();

            lista.Insert(0, new SelectListItem()
            {
                Text = "Seleccionar Estado",
                Value = ""
            });

            lista.Add(new SelectListItem()
            {
                Text = "Nuevo",
                Value = "Nuevo"
            });

            lista.Add(new SelectListItem()
            {
                Text = "Aceptado",
                Value = "Aceptado"
            });

            lista.Add(new SelectListItem()
            {
                Text = "Rechazado",
                Value = "Rechazado"
            });

            return lista;

        }

        protected List<SelectListItem> ObtenerEspeciesSelect()
        {
            var lista = new List<SelectListItem>();

            lista.Insert(0, new SelectListItem()
            {
                Text = "Seleccionar Especie",
                Value = ""
            });

            lista.Add(new SelectListItem()
            {
                Text = "Perro",
                Value = "Perro"
            });

            lista.Add(new SelectListItem()
            {
                Text = "Gato",
                Value = "Gato"
            });

            return lista;

        }

        protected SolicitudAdopcionViewModel ObtenerSolicitudDeModelo(SolicitudAdopcion entidad)
        {

            var viewModel = new SolicitudAdopcionViewModel()
            {
                ApellidoMaterno = entidad.ApellidoMaterno,
                ApellidoPaterno = entidad.ApellidoPaterno,
                Direccion = entidad.Direccion,
                Estado = entidad.Estado,
                Fecha = entidad.Fecha.ToString("yyyy-MM-dd"),
                MascotaId = entidad.MascotaId,
                Motivo = entidad.Motivo,
                Nombres = entidad.Nombres,
                SolicitudAdopcionId = entidad.SolicitudAdopcionId,
                Username = entidad.Username,
                Mascota = entidad.Mascota
            };

            return viewModel;

        }

        protected SolicitudAdopcion ObtenerSolicitudDeViewModel(SolicitudAdopcionViewModel viewModel)
        {

            DateTime fecha = DateTime.MinValue;

            DateTime.TryParseExact(viewModel.Fecha, "yyyy-MM-dd",
                   System.Globalization.CultureInfo.InvariantCulture,
                   System.Globalization.DateTimeStyles.None, out fecha);

            if (fecha == DateTime.MinValue)
            {
                fecha = DateTime.Now;
            }


            var entidad = new SolicitudAdopcion()
            {
                ApellidoMaterno = viewModel.ApellidoMaterno,
                ApellidoPaterno = viewModel.ApellidoPaterno,
                Direccion = viewModel.Direccion,
                Estado = viewModel.Estado,
                Fecha = fecha,
                MascotaId = viewModel.MascotaId,
                Motivo = viewModel.Motivo,
                Nombres = viewModel.Nombres,
                SolicitudAdopcionId = viewModel.SolicitudAdopcionId,
                Username = viewModel.Username

            };

            return entidad;

        }


        protected MascotaViewModel ObtenerMascotaDeModelo(Mascota mascota)
        {
            if (mascota == null)
            {
                return null;
            }

            var viewModel = new MascotaViewModel()
            {
                Color = mascota.Color,
                Especie = mascota.Especie,
                EstaAdoptado = mascota.EstaAdoptado,
                Foto = mascota.Foto,
                MascotaId = mascota.MascotaId,
                Nombre = mascota.Nombre,
                Raza = mascota.Raza,
                Sexo = mascota.Sexo,
                Nacimiento = mascota.Nacimiento.HasValue ? mascota.Nacimiento.Value.ToString("yyyy-MM-dd") : null,
                UrlVideoYoutube = mascota.UrlVideoYoutube,
                Descripcion = mascota.Descripcion
            };


            return viewModel;
        }

        protected Mascota ObtenerMascotaDeViewModel(MascotaViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            var modelo = new Mascota()
            {
                Color = viewModel.Color,
                Especie = viewModel.Especie,
                EstaAdoptado = viewModel.EstaAdoptado,
                Foto = viewModel.Foto,
                MascotaId = viewModel.MascotaId,
                Nombre = viewModel.Nombre,
                Raza = viewModel.Raza,
                Sexo = viewModel.Sexo,
                UrlVideoYoutube = viewModel.UrlVideoYoutube,
                Descripcion = viewModel.Descripcion
            };

            DateTime fecha = DateTime.MinValue;

            DateTime.TryParseExact(viewModel.Nacimiento, "yyyy-MM-dd",
                   System.Globalization.CultureInfo.InvariantCulture,
                   System.Globalization.DateTimeStyles.None, out fecha);

            if (fecha != DateTime.MinValue)
            {
                modelo.Nacimiento = fecha;
            }

            return modelo;
        }

        protected string ObtenerUsernameActual()
        {
            var username = default(string);
            var claims = ((ClaimsIdentity)User.Identity).Claims;
            var claim = claims.SingleOrDefault(m => m.Type == ClaimTypes.NameIdentifier);

            if (claim != null)
            {
                username = claim.Value;
            }

            return username;
        }

        protected List<SelectListItem> ObtenerSexos()
        {
            var lista = new List<SelectListItem>();

            lista.Insert(0, new SelectListItem()
            {
                Text = "Seleccionar Sexo",
                Value = ""
            });

            lista.Add(new SelectListItem()
            {
                Text = "Hembra",
                Value = "H"
            });

            lista.Add(new SelectListItem()
            {
                Text = "Macho",
                Value = "M"
            });


            return lista;

        }

        protected void EnviarCorreo(string titulo, string contenido, List<string> destinatarios)
        {
            var cliente = new SmtpClient()
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                UseDefaultCredentials = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential("envioemailpruebas2020@gmail.com", "developer2022*")
            };

            var mailMessage = new MailMessage
            {
                From = new MailAddress("envioemailpruebas2020@gmail.com"),
                Subject = titulo,
                Body = contenido,
                IsBodyHtml = true,
            };

            destinatarios.ForEach(e =>
            {
                mailMessage.To.Add(e);
            });


            try
            {
                cliente.Send(mailMessage);
            }
            catch { }
        }

    }
}