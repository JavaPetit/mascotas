﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Zurita.Mvc.ViewModels.Mascota;
using Zurita.UnitOfWork;

namespace Zurita.Mvc.Controllers
{
    public class HomeController : BaseController
    {

        public HomeController(ILog log, IUnitOfWork unit) : base(log, unit)
        {

        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Sexos = ObtenerSexos();
            ViewBag.Especies = ObtenerEspeciesSelect();

            return View();
        }

        [AllowAnonymous]
        public async Task<PartialViewResult> Mascotas(string especie = null, string sexo = null)
        {
            var lista = await Buscar(especie, sexo);

            return PartialView(lista);
        }

        public async Task<List<MascotaViewModel>> Buscar(string especie = null, string sexo = null)
        {
            var httpClient = new HttpClient();

            UriBuilder builder = new UriBuilder($"{ConfigurationManager.AppSettings["base_servicios_web"]}api/Mascota/Listar");
            builder.Query = $"especie={especie}&sexo={sexo}";

            var respuesta = await httpClient.GetAsync(builder.Uri);
            respuesta.EnsureSuccessStatusCode();

            string json = await respuesta.Content.ReadAsStringAsync();

            var lista = JsonConvert.DeserializeObject<List<MascotaViewModel>>(json);

            return lista;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}