﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Zurita.Models;
using Zurita.Mvc.ViewModels.Mascota;
using Zurita.UnitOfWork;

namespace Zurita.Mvc.Controllers
{
    public class MascotaController : BaseController
    {

        public MascotaController(ILog log, IUnitOfWork unit) : base(log, unit)
        {
        }

        // GET: Mascota
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult Lista()
        {
            var modelos = _unit.Mascotas.GetList();
            var lista = new List<MascotaViewModel>();

            foreach (var modelo in modelos)
            {
                lista.Add(ObtenerMascotaDeModelo(modelo));
            }

            return PartialView(lista);
        }

        [HttpGet]
        public PartialViewResult Create()
        {
            ViewBag.Sexos = ObtenerSexos();
            ViewBag.Especies = ObtenerEspeciesSelect();
            return PartialView();
        }

        [HttpPost]
        public ActionResult Create(MascotaViewModel viewModel)
        {
            if (ModelState.IsValid)
            {

                var mascota = ObtenerMascotaDeViewModel(viewModel);

                _unit.Mascotas.Insert(mascota);
                return RedirectToAction("Index");
            }

            return PartialView(viewModel);
        }


        [HttpGet]
        public PartialViewResult Edit(int id)
        {
            ViewBag.Sexos = ObtenerSexos();
            ViewBag.Especies = ObtenerEspeciesSelect();


            return PartialView(ObtenerMascotaDeModelo(_unit.Mascotas.GetById(id)));
        }

        [HttpPost]
        public ActionResult Edit(MascotaViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var mascota = ObtenerMascotaDeViewModel(viewModel);

            if (_unit.Mascotas.Update(mascota))
            {
                return RedirectToAction("Index");
            }
            return PartialView(viewModel);
        }


        [HttpGet]
        public PartialViewResult Delete(int id)
        {
            return PartialView(ObtenerMascotaDeModelo(_unit.Mascotas.GetById(id)));
        }


        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.Mascotas.Delete(id))
            {
                return RedirectToAction("Index");
            }

            return PartialView(ObtenerMascotaDeModelo(_unit.Mascotas.GetById(id)));
        }

        [HttpGet]
        public PartialViewResult Details(int id)
        {
            return PartialView(ObtenerMascotaDeModelo(_unit.Mascotas.GetById(id)));
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult DetallePublico(int id)
        {
            ViewBag.SolicitudActual = 0;
            ViewBag.EstadoActual = "";
            if (Request.IsAuthenticated)
            {
                var solicitud = _unit.SolicitudesAdopcion.ObtenerPorMascotaYUsuario(id, ObtenerUsernameActual());
                if (solicitud != null)
                {
                    ViewBag.SolicitudActual = solicitud.SolicitudAdopcionId;
                    ViewBag.EstadoActual = solicitud.Estado;
                }
            }
            return View(ObtenerMascotaDeModelo(_unit.Mascotas.GetById(id)));
        }

        [HttpPost]
        public ActionResult SubirFoto()
        {

            string path = System.Web.HttpContext.Current.Server.MapPath("~/Subidas/");

            HttpPostedFile postedFile = System.Web.HttpContext.Current.Request.Files[0];

            var nombreArchivo = Guid.NewGuid() + Path.GetExtension(postedFile.FileName);

            postedFile.SaveAs(path + nombreArchivo);
            return Json(new
            {
                nombreArchivo
            });
        }

        [HttpPost]
        public ActionResult SolicitarAdopcion(PeticionAdoptarMascotaViewModel peticion)
        {
            var username = ObtenerUsernameActual();
            var usuario = _unit.Usuarios.BuscarPorUsuario(username);
            var solicitudAdocion = new SolicitudAdopcion()
            {
                Username = username,
                MascotaId = peticion.IdMascota,
                ApellidoMaterno = usuario.Apellidos,
                ApellidoPaterno = usuario.Apellidos,
                Direccion = usuario.Direccion,
                Estado = "Nuevo",
                Fecha = DateTime.Now,
                Motivo = "Adopción",
                Nombres = usuario.Nombres
            };

            var id = _unit.SolicitudesAdopcion.Insert(solicitudAdocion);


            var cuerpoEmail = $"Hola {usuario.Nombres} hemos recibido tu solicitud de adopcion";

            EnviarCorreo("Solicitud recibida", cuerpoEmail, new List<string>() { usuario.Correo });

            return Json(new
            {
                id,
                suceso = true
            });
        }

        [HttpPost]
        public ActionResult CancelarSolicitud(PeticionCancelarSolicitudAdopcionViewModel peticion)
        {
            var username = ObtenerUsernameActual();
            var usuario = _unit.Usuarios.BuscarPorUsuario(username);


            _unit.SolicitudesAdopcion.Delete(peticion.IdSolicitudAdopcion);


            var cuerpoEmail = $"Hola {usuario.Nombres} hemos recibido tu solicitud de cancelación de la adopción";

            EnviarCorreo("Solicitud cancelada", cuerpoEmail, new List<string>() { usuario.Correo });

            return Json(new
            {
                suceso = true
            });
        }


    }
}