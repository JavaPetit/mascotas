﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Zurita.Mvc.ViewModels.MisAdopciones;
using Zurita.Mvc.ViewModels.SolicitudAdopcion;
using Zurita.UnitOfWork;

namespace Zurita.Mvc.Controllers
{
    public class MisAdopcionesController : BaseController
    {


        public MisAdopcionesController(ILog log, IUnitOfWork unit) : base(log, unit)
        {
        }

        // GET: MisAdopciones
        public ActionResult Index()
        {

            var lista = new List<SolicitudAdopcionViewModel>();

            foreach (var entidad in _unit.SolicitudesAdopcion.GetList(ObtenerUsernameActual()))
            {
                lista.Add(ObtenerSolicitudDeModelo(entidad));
            }

            return View(lista);
        }

        [HttpGet]
        public ActionResult Detalle(int id)
        {
            var solicitud = _unit.SolicitudesAdopcion.GetById(id);
            if (solicitud == null)
            {
                return RedirectToAction("Index");
            }

            var mascota = _unit.Mascotas.GetById(solicitud.MascotaId.Value);

            var viewModel = new MiAdopcionViewModel()
            {
                Mascota = ObtenerMascotaDeModelo(mascota),
                Solicitud = ObtenerSolicitudDeModelo(solicitud)
            };


            return View(viewModel);
        }
    }
}