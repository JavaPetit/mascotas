﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Zurita.Models;
using Zurita.Mvc.ViewModels.SolicitudAdopcion;
using Zurita.UnitOfWork;

namespace Zurita.Mvc.Controllers
{
    public class SolicitudAdopcionController : BaseController
    {

        public SolicitudAdopcionController(ILog log, IUnitOfWork unit) : base(log, unit)
        {
        }

        // GET: SolicitudAdopcion
        public ActionResult Index()
        {
            var lista = new List<SolicitudAdopcionViewModel>();

            foreach (var entidad in _unit.SolicitudesAdopcion.GetList())
            {
                lista.Add(ObtenerSolicitudDeModelo(entidad));
            }

            return View(lista);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Mascotas = ObtenerMascotasSelect();
            ViewBag.Estados = ObtenerEstadosSolicitudSelect();
            ViewBag.Usuarios = ObtenerUsuariosSelect();
            return View();
        }

        [HttpPost]
        public ActionResult Create(SolicitudAdopcionViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var entidad = ObtenerSolicitudDeViewModel(viewModel);

                _unit.SolicitudesAdopcion.Insert(entidad);
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            var entidad = _unit.SolicitudesAdopcion.GetById(id);

            var viewModel = ObtenerSolicitudDeModelo(entidad);

            ViewBag.Mascotas = ObtenerMascotasSelect();
            ViewBag.Estados = ObtenerEstadosSolicitudSelect();
            ViewBag.Usuarios = ObtenerUsuariosSelect();


            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(SolicitudAdopcionViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var entidad = ObtenerSolicitudDeViewModel(viewModel);

                if (_unit.SolicitudesAdopcion.Update(entidad))
                {
                    if (entidad.Estado.Equals("Aceptado", StringComparison.OrdinalIgnoreCase))
                    {
                        var usuario = _unit.Usuarios.BuscarPorUsuario(entidad.Username);
                        var cuerpoEmail = $"Hola {usuario.Nombres} tu solicitud fue aceptada";

                        EnviarCorreo("Solicitud aceptada", cuerpoEmail, new List<string>() { usuario.Correo });
                    }

                    if (entidad.Estado.Equals("Rechazado", StringComparison.OrdinalIgnoreCase))
                    {
                        var usuario = _unit.Usuarios.BuscarPorUsuario(entidad.Username);
                        var cuerpoEmail = $"Hola {usuario.Nombres} tu solicitud fue aceptada";

                        EnviarCorreo("Solicitud rechazada", cuerpoEmail, new List<string>() { usuario.Correo });
                    }

                    return RedirectToAction("Index");
                }

                return RedirectToAction("Index");
            }


            return View(viewModel);
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            var entidad = _unit.SolicitudesAdopcion.GetById(id);

            var viewModel = ObtenerSolicitudDeModelo(entidad);

            return View(viewModel);
        }


        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.SolicitudesAdopcion.Delete(id))
            {
                return RedirectToAction("Index");
            }

            var entidad = _unit.SolicitudesAdopcion.GetById(id);

            var viewModel = ObtenerSolicitudDeModelo(entidad);

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            var entidad = _unit.SolicitudesAdopcion.GetById(id);

            var viewModel = ObtenerSolicitudDeModelo(entidad);

            return View(viewModel);
        }




    }
}