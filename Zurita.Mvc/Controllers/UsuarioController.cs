﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Zurita.Models;
using Zurita.Mvc.ViewModels.Usuario;
using Zurita.UnitOfWork;

namespace Zurita.Mvc.Controllers
{
    public class UsuarioController : BaseController
    {


        public UsuarioController(ILog log, IUnitOfWork unit) : base(log, unit)
        {
        }

        // GET: Usuario
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult Lista()
        {
            var modelos = _unit.Usuarios.GetList();
            var lista = new List<UsuarioViewModel>();

            foreach (var modelo in modelos)
            {
                lista.Add(ObtenerDeModelo(modelo));
            }

            return PartialView(lista);
        }

        [HttpGet]
        public PartialViewResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Create(UsuarioViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var usuario = ObtenerDeViewModel(viewModel);

                _unit.Usuarios.Insert(usuario);
                return RedirectToAction("Index");
            }

            return PartialView(viewModel);
        }

        [HttpGet]
        public PartialViewResult Edit(string id)
        {

            return PartialView(ObtenerDeModelo(_unit.Usuarios.BuscarPorUsuario(id)));
        }

        [HttpPost]
        public ActionResult Edit(UsuarioViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }

            var usuario = ObtenerDeViewModel(viewModel);

            if (_unit.Usuarios.Update(usuario))
            {
                return RedirectToAction("Index");
            }
            return PartialView(viewModel);
        }


        [HttpGet]
        public PartialViewResult Delete(string id)
        {
            return PartialView(ObtenerDeModelo(_unit.Usuarios.BuscarPorUsuario(id)));
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(string id)
        {
            if (_unit.Usuarios.Delete(id))
            {
                return RedirectToAction("Index");
            }

            return PartialView(ObtenerDeModelo(_unit.Usuarios.BuscarPorUsuario(id)));
        }

        [HttpGet]
        public PartialViewResult Details(string id)
        {
            return PartialView(ObtenerDeModelo(_unit.Usuarios.BuscarPorUsuario(id)));
        }

        private UsuarioViewModel ObtenerDeModelo(Usuario usuario)
        {
            if (usuario == null)
            {
                return null;
            }

            var viewModel = new UsuarioViewModel()
            {
                Apellidos = usuario.Apellidos,
                Email = usuario.Correo,
                Direccion = usuario.Direccion,
                EsAdministrador = usuario.EsAdministrador,
                Nombres = usuario.Nombres,
                Password = usuario.Password,
                Telefono = usuario.Telefono,
                Username = usuario.Username
            };


            return viewModel;
        }

        private Usuario ObtenerDeViewModel(UsuarioViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            var modelo = new Usuario()
            {
                Apellidos = viewModel.Apellidos,
                Correo = viewModel.Email,
                Direccion = viewModel.Direccion,
                EsAdministrador = viewModel.EsAdministrador,
                Nombres = viewModel.Nombres,
                Password = viewModel.Password,
                Telefono = viewModel.Telefono,
                Username = viewModel.Username
            };
            return modelo;
        }
    }
}