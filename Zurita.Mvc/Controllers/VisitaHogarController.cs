﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Zurita.Models;
using Zurita.Mvc.ViewModels.VisitaHogar;
using Zurita.UnitOfWork;

namespace Zurita.Mvc.Controllers
{
    public class VisitaHogarController : BaseController
    {

        public VisitaHogarController(ILog log, IUnitOfWork unit) : base(log, unit)
        {
        }

        // GET: VisitaHogar
        public ActionResult Index()
        {
            var lista = new List<VisitaHogarViewModel>();

            foreach (var entidad in _unit.VisitasHogar.GetList())
            {
                lista.Add(new VisitaHogarViewModel()
                {
                    Direccion = entidad.Direccion,
                    Fecha = entidad.Fecha,
                    MascotaId = entidad.MascotaId,
                    Motivo = entidad.Motivo,
                    Observacion = entidad.Observacion,
                    VisitaHogarId = entidad.VisitaHogarId
                });
            }

            return View(lista);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Mascotas = ObtenerMascotasSelect();
            return View();
        }

        [HttpPost]
        public ActionResult Create(VisitaHogarViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var entidad = new VisitaHogar()
                {
                    Direccion = viewModel.Direccion,
                    Fecha = viewModel.Fecha,
                    MascotaId = viewModel.MascotaId,
                    Motivo = viewModel.Motivo,
                    Observacion = viewModel.Observacion,
                    VisitaHogarId = viewModel.VisitaHogarId
                };

                _unit.VisitasHogar.Insert(entidad);
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var entidad = _unit.VisitasHogar.GetById(id);

            var viewModel = new VisitaHogarViewModel()
            {
                Direccion = entidad.Direccion,
                Fecha = entidad.Fecha,
                MascotaId = entidad.MascotaId,
                Motivo = entidad.Motivo,
                Observacion = entidad.Observacion,
                VisitaHogarId = entidad.VisitaHogarId
            };

            ViewBag.Mascotas = ObtenerMascotasSelect();

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(VisitaHogarViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var entidad = new VisitaHogar()
                {
                    Direccion = viewModel.Direccion,
                    Fecha = viewModel.Fecha,
                    MascotaId = viewModel.MascotaId,
                    Motivo = viewModel.Motivo,
                    Observacion = viewModel.Observacion,
                    VisitaHogarId = viewModel.VisitaHogarId
                };

                if (_unit.VisitasHogar.Update(entidad))
                {
                    return RedirectToAction("Index");
                }

                return RedirectToAction("Index");
            }


            return View(viewModel);
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            var entidad = _unit.VisitasHogar.GetById(id);

            var viewModel = new VisitaHogarViewModel()
            {
                Direccion = entidad.Direccion,
                Fecha = entidad.Fecha,
                MascotaId = entidad.MascotaId,
                Motivo = entidad.Motivo,
                Observacion = entidad.Observacion,
                VisitaHogarId = entidad.VisitaHogarId
            };

            return View(viewModel);
        }


        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.VisitasHogar.Delete(id))
            {
                return RedirectToAction("Index");
            }
            var entidad = _unit.VisitasHogar.GetById(id);

            var viewModel = new VisitaHogarViewModel()
            {
                Direccion = entidad.Direccion,
                Fecha = entidad.Fecha,
                MascotaId = entidad.MascotaId,
                Motivo = entidad.Motivo,
                Observacion = entidad.Observacion,
                VisitaHogarId = entidad.VisitaHogarId
            };

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            var entidad = _unit.VisitasHogar.GetById(id);

            var viewModel = new VisitaHogarViewModel()
            {
                Direccion = entidad.Direccion,
                Fecha = entidad.Fecha,
                MascotaId = entidad.MascotaId,
                Motivo = entidad.Motivo,
                Observacion = entidad.Observacion,
                VisitaHogarId = entidad.VisitaHogarId
            };

            return View(viewModel);
        }

    }
}