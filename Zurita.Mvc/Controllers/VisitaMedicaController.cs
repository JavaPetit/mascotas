﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using Zurita.Models;
using Zurita.Mvc.ViewModels.VisitaMedica;
using Zurita.UnitOfWork;

namespace Zurita.Mvc.Controllers
{
    public class VisitaMedicaController : BaseController
    {

        public VisitaMedicaController(ILog log, IUnitOfWork unit) : base(log, unit)
        {
        }

        // GET: Usuario
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult Lista()
        {
            var lista = new List<VisitaMedicaViewModel>();

            foreach (var modelo in _unit.VisitasMedicas.GetList())
            {
                lista.Add(ObtenerDeModelo(modelo));
            }

            return PartialView(lista);
        }

        [HttpGet]
        public PartialViewResult Create()
        {
            ViewBag.Mascotas = ObtenerMascotasSelect();
            return PartialView();
        }

        [HttpPost]
        public ActionResult Create(VisitaMedicaViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var entidad = ObtenerDeViewModel(viewModel);

                _unit.VisitasMedicas.Insert(entidad);
                return RedirectToAction("Index");
            }

            return PartialView(viewModel);
        }

        [HttpGet]
        public PartialViewResult Edit(int id)
        {
            ViewBag.Mascotas = ObtenerMascotasSelect();

            return PartialView(ObtenerDeModelo(_unit.VisitasMedicas.GetById(id)));
        }

        [HttpPost]
        public ActionResult Edit(VisitaMedicaViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(viewModel);
            }
            var entidad = ObtenerDeViewModel(viewModel);

            if (_unit.VisitasMedicas.Update(entidad))
            {
                return RedirectToAction("Index");
            }

            return PartialView(viewModel);
        }


        [HttpGet]
        public PartialViewResult Delete(int id)
        {

            return PartialView(ObtenerDeModelo(_unit.VisitasMedicas.GetById(id)));
        }


        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.VisitasMedicas.Delete(id))
            {
                return RedirectToAction("Index");
            }
            return PartialView(ObtenerDeModelo(_unit.VisitasMedicas.GetById(id)));
        }

        [HttpGet]
        public PartialViewResult Details(int id)
        {
            return PartialView(ObtenerDeModelo(_unit.VisitasMedicas.GetById(id)));
        }


        private VisitaMedicaViewModel ObtenerDeModelo(VisitaMedica modelo)
        {
            if (modelo == null)
            {
                return null;
            }

            var viewModel = new VisitaMedicaViewModel()
            {
                CondicionCorporal = modelo.CondicionCorporal,
                Diagnostico = modelo.Diagnostico,
                Fecha = modelo.Fecha.ToString("yyyy-MM-dd"),
                MascotaId = modelo.MascotaId,
                Medico = modelo.Medico,
                Motivo = modelo.Motivo,
                Tratamiento = modelo.Tratamiento,
                VisitaMedicaId = modelo.VisitaMedicaId
            };


            return viewModel;
        }

        private VisitaMedica ObtenerDeViewModel(VisitaMedicaViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            var modelo = new VisitaMedica()
            {
                CondicionCorporal = viewModel.CondicionCorporal,
                Diagnostico = viewModel.Diagnostico,
                MascotaId = viewModel.MascotaId,
                Medico = viewModel.Medico,
                Motivo = viewModel.Motivo,
                Tratamiento = viewModel.Tratamiento,
                VisitaMedicaId = viewModel.VisitaMedicaId
            };

            DateTime fecha = DateTime.MinValue;

            DateTime.TryParseExact(viewModel.Fecha, "yyyy-MM-dd",
                   System.Globalization.CultureInfo.InvariantCulture,
                   System.Globalization.DateTimeStyles.None, out fecha);

            if (fecha != DateTime.MinValue)
            {
                modelo.Fecha = fecha;
            }


            return modelo;
        }

    }
}