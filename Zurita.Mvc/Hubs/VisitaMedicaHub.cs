﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Zurita.Mvc.Hubs
{
    public class VisitaMedicaHub : Hub
    {
        static List<int> VisitaMedicaIds = new List<int>();

        public void AddVisitaMedicaId(int id)
        {
            if (!VisitaMedicaIds.Contains(id))
            {
                VisitaMedicaIds.Add(id);
            }
            Clients.All.visitaMedicaStatus(VisitaMedicaIds);
        }

        public void RemoveVisitaMedicaId(int id)
        {
            if (VisitaMedicaIds.Contains(id)) VisitaMedicaIds.Remove(id);
            Clients.All.visitaMedicaStatus(VisitaMedicaIds);
        }

        public override Task OnConnected()
        {
            return Clients.All.visitaMedicaStatus(VisitaMedicaIds);
        }

        public void Message(string message)
        {
            Clients.All.getMessage(message);
        }
    }
}