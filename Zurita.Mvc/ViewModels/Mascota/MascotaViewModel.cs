﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Zurita.Mvc.ViewModels.Mascota
{
    public class MascotaViewModel
    {


        public int MascotaId { get; set; }

        [Required]
        [StringLength(400, ErrorMessage = "El número de caracteres debe de {0} de ser al menos {2}", MinimumLength = 2)]
        public string Nombre { get; set; }

        public string Nacimiento { get; set; }

        public string Color { get; set; }

        [Required]
        public string Especie { get; set; }

        public string Raza { get; set; }

        public string Sexo { get; set; }

        public bool EstaAdoptado { get; set; }

        public string Foto { get; set; }

        public string UrlVideoYoutube { get; set; }

        [MaxLength(500, ErrorMessage = "Como máximo admite 500 caracteres")]
        public string Descripcion { get; set; }
    }
}