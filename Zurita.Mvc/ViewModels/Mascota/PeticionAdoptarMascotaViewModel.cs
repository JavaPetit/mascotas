﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zurita.Mvc.ViewModels.Mascota
{
    public class PeticionAdoptarMascotaViewModel
    {

        [JsonProperty(PropertyName = "id")]
        public int IdMascota { get; set; }
    }
}