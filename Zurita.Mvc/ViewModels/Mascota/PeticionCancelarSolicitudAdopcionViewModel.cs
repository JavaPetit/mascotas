﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Zurita.Mvc.ViewModels.Mascota
{
    public class PeticionCancelarSolicitudAdopcionViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public int IdSolicitudAdopcion { get; set; }
    }
}