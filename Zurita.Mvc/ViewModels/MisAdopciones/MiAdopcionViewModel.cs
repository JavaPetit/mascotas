﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Zurita.Mvc.ViewModels.Mascota;
using Zurita.Mvc.ViewModels.SolicitudAdopcion;

namespace Zurita.Mvc.ViewModels.MisAdopciones
{
    public class MiAdopcionViewModel
    {

        public MascotaViewModel Mascota { get; set; }

        public SolicitudAdopcionViewModel Solicitud { get; set; }
    }
}