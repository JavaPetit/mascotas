﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Zurita.Mvc.ViewModels.SolicitudAdopcion
{
    public class SolicitudAdopcionViewModel
    {

        public int SolicitudAdopcionId { get; set; }

        [Required]
        [StringLength(400, ErrorMessage = "El número de caracteres debe de {0} de ser al menos {2}", MinimumLength = 2)]
        public string Motivo { get; set; }

        [Required]
        public string Fecha { get; set; }

        public string Direccion { get; set; }

        [Required]
        [StringLength(400, ErrorMessage = "El número de caracteres debe de {0} de ser al menos {2}", MinimumLength = 2)]
        public string Nombres { get; set; }

        [Required]
        [StringLength(400, ErrorMessage = "El número de caracteres debe de {0} de ser al menos {2}", MinimumLength = 2)]
        public string ApellidoPaterno { get; set; }

        [Required]
        [StringLength(400, ErrorMessage = "El número de caracteres debe de {0} de ser al menos {2}", MinimumLength = 2)]
        public string ApellidoMaterno { get; set; }

        [Required]
        public string Estado { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Valor para {0} debe de ser entre {1} y {2}.")]
        public int? MascotaId { get; set; }

        [Required]
        public string Username { get; set; }

        public string Mascota { get; set; }
    }
}