﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Zurita.Mvc.ViewModels.Usuario
{
    public class UsuarioViewModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Nombres { get; set; }

        [Required]
        public string Apellidos { get; set; }

        [Required]
        public string Telefono { get; set; }

        [Required]
        public string Direccion { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Correo Inválido")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool EsAdministrador { get; set; }
    }
}