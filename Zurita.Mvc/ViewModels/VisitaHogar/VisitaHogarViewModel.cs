﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Zurita.Mvc.ViewModels.VisitaHogar
{
    public class VisitaHogarViewModel
    {

        public int VisitaHogarId { get; set; }

        [Required]
        [StringLength(400, ErrorMessage = "El número de caracteres debe de {0} de ser al menos {2}", MinimumLength = 2)]
        public string Motivo { get; set; }

        [Required]
        public DateTime Fecha { get; set; }

        [StringLength(400, ErrorMessage = "El número de caracteres debe de {0} de ser al menos {2}", MinimumLength = 2)]
        public string Direccion { get; set; }

        [StringLength(1000, ErrorMessage = "El número de caracteres debe de {0} de ser al menos {2}", MinimumLength = 2)]
        public string Observacion { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Valor para {0} debe de ser entre {1} y {2}.")]
        public int? MascotaId { get; set; }
    }
}