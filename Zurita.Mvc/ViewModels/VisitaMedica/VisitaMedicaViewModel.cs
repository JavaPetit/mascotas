﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Zurita.Mvc.ViewModels.VisitaMedica
{
    public class VisitaMedicaViewModel
    {
        public int VisitaMedicaId { get; set; }

        [Required]
        [StringLength(400, ErrorMessage = "El número de caracteres debe de {0} de ser al menos {2}", MinimumLength = 2)]
        public string Motivo { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "El número de caracteres debe de {0} de ser al menos {2}", MinimumLength = 2)]
        public string Medico { get; set; }

        [Required]
        public String Fecha { get; set; }

        [StringLength(400, ErrorMessage = "El número de caracteres debe de {0} de ser al menos {2}", MinimumLength = 2)]
        public string CondicionCorporal { get; set; }

        [StringLength(600, ErrorMessage = "El número de caracteres debe de {0} de ser al menos {2}", MinimumLength = 2)]
        public string Diagnostico { get; set; }

        [StringLength(600, ErrorMessage = "El número de caracteres debe de {0} de ser al menos {2}", MinimumLength = 2)]
        public string Tratamiento { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Valor para {0} debe de ser entre {1} y {2}.")]
        public int? MascotaId { get; set; }
    }
}