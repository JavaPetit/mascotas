﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zurita.Models;
using Zurita.Repositories.Mascotas;
using Dapper;
using Dapper.Contrib.Extensions;

namespace Zurita.Repositories.Dapper.Mascotas
{
    public class MascotaRepository : Repository<Mascota>, IMascotaRepository
    {

        public MascotaRepository(string connectionString) : base(connectionString) { }

        public override IEnumerable<Mascota> GetList()
        {
            return GetList(null, null);
        }

        public IEnumerable<Mascota> GetList(string especie, string genero)
        {
            var p = new DynamicParameters();

            p.Add("@Especie", !string.IsNullOrWhiteSpace(especie) ? especie : null);
            p.Add("@Sexo", !string.IsNullOrWhiteSpace(genero) ? genero : null);

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Mascota>("usp_Mascota_Listar", p, commandType: System.Data.CommandType.StoredProcedure).ToList();
            }
        }

        public override Mascota GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Mascota>("usp_Mascota_Obtener", new
                {
                    MascotaId = id
                }, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public override bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Execute("usp_Mascota_Eliminar", new
                {
                    MascotaId = id
                }, commandType: System.Data.CommandType.StoredProcedure);
            }

            return true;
        }

        public override int Insert(Mascota entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {


                var p = new DynamicParameters();

                p.Add("@Nombre", entity.Nombre);
                p.Add("@Nacimiento", entity.Nacimiento ?? null);
                p.Add("@Color", !string.IsNullOrWhiteSpace(entity.Color) ? entity.Color : null);
                p.Add("@Especie", !string.IsNullOrWhiteSpace(entity.Especie) ? entity.Especie : null);
                p.Add("@Raza", !string.IsNullOrWhiteSpace(entity.Raza) ? entity.Raza : null);
                p.Add("@Sexo", !string.IsNullOrWhiteSpace(entity.Sexo) ? entity.Sexo : null);
                p.Add("@EstaAdoptado", entity.EstaAdoptado);
                p.Add("@Foto", !string.IsNullOrWhiteSpace(entity.Foto) ? entity.Foto : null);
                p.Add("@UrlVideoYoutube", !string.IsNullOrWhiteSpace(entity.UrlVideoYoutube) ? entity.UrlVideoYoutube : null);
                p.Add("@Descripcion", !string.IsNullOrWhiteSpace(entity.Descripcion) ? entity.Descripcion : null);

                var objeto = connection.ExecuteScalar("usp_Mascota_Insertar",
                    p, commandType: System.Data.CommandType.StoredProcedure);

                return Convert.ToInt32(objeto);
            }
        }

        public override bool Update(Mascota entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var p = new DynamicParameters();

                p.Add("@MascotaId", entity.MascotaId);
                p.Add("@Nombre", entity.Nombre);
                p.Add("@Nacimiento", entity.Nacimiento ?? null);
                p.Add("@Color", !string.IsNullOrWhiteSpace(entity.Color) ? entity.Color : null);
                p.Add("@Especie", !string.IsNullOrWhiteSpace(entity.Especie) ? entity.Especie : null);
                p.Add("@Raza", !string.IsNullOrWhiteSpace(entity.Raza) ? entity.Raza : null);
                p.Add("@Sexo", !string.IsNullOrWhiteSpace(entity.Sexo) ? entity.Sexo : null);
                p.Add("@EstaAdoptado", entity.EstaAdoptado);
                p.Add("@Foto", !string.IsNullOrWhiteSpace(entity.Foto) ? entity.Foto : null);
                p.Add("@UrlVideoYoutube", !string.IsNullOrWhiteSpace(entity.UrlVideoYoutube) ? entity.UrlVideoYoutube : null);
                p.Add("@Descripcion", !string.IsNullOrWhiteSpace(entity.Descripcion) ? entity.Descripcion : null);


                connection.Execute("usp_Mascota_Actualizar", p, commandType: System.Data.CommandType.StoredProcedure);

                return true;
            }
        }


    }
}
