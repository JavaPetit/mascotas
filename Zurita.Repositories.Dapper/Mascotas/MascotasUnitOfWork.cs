﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zurita.Repositories.Mascotas;
using Zurita.UnitOfWork;

namespace Zurita.Repositories.Dapper.Mascotas
{
    public class MascotasUnitOfWork : IUnitOfWork
    {

        public MascotasUnitOfWork(string connectionString)
        {
            Mascotas = new MascotaRepository(connectionString);
            SolicitudesAdopcion = new SolicitudAdopcionRepository(connectionString);
            VisitasMedicas = new VisitaMedicaRepository(connectionString);
            VisitasHogar = new VisitaHogarRepository(connectionString);
            Usuarios = new UsuarioRepository(connectionString);
        }


        public IMascotaRepository Mascotas { get; private set; }

        public ISolicitudAdopcionRepository SolicitudesAdopcion { get; private set; }

        public IVisitaMedicaRepository VisitasMedicas { get; private set; }

        public IVisitaHogarRepository VisitasHogar { get; private set; }

        public IUsuarioRepository Usuarios { get; private set; }
    }
}
