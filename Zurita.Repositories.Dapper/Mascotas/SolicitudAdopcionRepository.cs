﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zurita.Models;
using Zurita.Repositories.Mascotas;
using Dapper;
using Dapper.Contrib.Extensions;

namespace Zurita.Repositories.Dapper.Mascotas
{
    public class SolicitudAdopcionRepository : Repository<SolicitudAdopcion>, ISolicitudAdopcionRepository
    {
        public SolicitudAdopcionRepository(string connectionString) : base(connectionString) { }


        public override IEnumerable<SolicitudAdopcion> GetList()
        {
            return GetList(null);
        }

        public IEnumerable<SolicitudAdopcion> GetList(string username)
        {

            var p = new DynamicParameters();
            p.Add("@Username", !string.IsNullOrWhiteSpace(username) ? username : null);

            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<SolicitudAdopcion>("usp_SolicitudAdopcion_Listar", p, commandType: System.Data.CommandType.StoredProcedure).ToList();
            }
        }


        public override SolicitudAdopcion GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<SolicitudAdopcion>("usp_SolicitudAdopcion_Obtener", new
                {
                    SolicitudAdopcionId = id
                }, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public override bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Execute("usp_SolicitudAdopcion_Eliminar", new
                {
                    SolicitudAdopcionId = id
                }, commandType: System.Data.CommandType.StoredProcedure);
            }

            return true;
        }

        public override int Insert(SolicitudAdopcion entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {


                var p = new DynamicParameters();

                p.Add("@Motivo", !string.IsNullOrWhiteSpace(entity.Motivo) ? entity.Motivo : null);
                p.Add("@Fecha", entity.Fecha);
                p.Add("@Direccion", !string.IsNullOrWhiteSpace(entity.Direccion) ? entity.Direccion : null);
                p.Add("@Nombres", !string.IsNullOrWhiteSpace(entity.Nombres) ? entity.Nombres : null);
                p.Add("@ApellidoPaterno", !string.IsNullOrWhiteSpace(entity.ApellidoPaterno) ? entity.ApellidoPaterno : null);
                p.Add("@ApellidoMaterno", !string.IsNullOrWhiteSpace(entity.ApellidoMaterno) ? entity.ApellidoMaterno : null);
                p.Add("@Estado", entity.Estado);
                p.Add("@MascotaId", entity.MascotaId ?? null);
                p.Add("@Username", !string.IsNullOrWhiteSpace(entity.Username) ? entity.Username : null);

                var objeto = connection.ExecuteScalar("usp_SolicitudAdopcion_Insertar",
                    p, commandType: System.Data.CommandType.StoredProcedure);

                return Convert.ToInt32(objeto);
            }
        }

        public override bool Update(SolicitudAdopcion entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var p = new DynamicParameters();

                p.Add("@SolicitudAdopcionId", entity.SolicitudAdopcionId);
                p.Add("@Motivo", !string.IsNullOrWhiteSpace(entity.Motivo) ? entity.Motivo : null);
                p.Add("@Fecha", entity.Fecha);
                p.Add("@Direccion", !string.IsNullOrWhiteSpace(entity.Direccion) ? entity.Direccion : null);
                p.Add("@Nombres", !string.IsNullOrWhiteSpace(entity.Nombres) ? entity.Nombres : null);
                p.Add("@ApellidoPaterno", !string.IsNullOrWhiteSpace(entity.ApellidoPaterno) ? entity.ApellidoPaterno : null);
                p.Add("@ApellidoMaterno", !string.IsNullOrWhiteSpace(entity.ApellidoMaterno) ? entity.ApellidoMaterno : null);
                p.Add("@Estado", entity.Estado);
                p.Add("@MascotaId", entity.MascotaId ?? null);
                p.Add("@Username", !string.IsNullOrWhiteSpace(entity.Username) ? entity.Username : null);

                connection.Execute("usp_SolicitudAdopcion_Actualizar", p, commandType: System.Data.CommandType.StoredProcedure);

                return true;
            }
        }

        public SolicitudAdopcion ObtenerPorMascotaYUsuario(int idMascota, string username)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<SolicitudAdopcion>("usp_SolicitudAdopcion_Obtener_solicitada", new
                {
                    MascotaId = idMascota,
                    Username = username
                }, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
            }
        }
    }
}
