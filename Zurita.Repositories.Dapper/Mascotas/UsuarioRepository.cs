﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zurita.Models;
using Zurita.Repositories.Mascotas;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Data.SqlClient;

namespace Zurita.Repositories.Dapper.Mascotas
{
    public class UsuarioRepository : Repository<Usuario>, IUsuarioRepository
    {

        public UsuarioRepository(string connectionString) : base(connectionString) { }


        public override IEnumerable<Usuario> GetList()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Usuario>("usp_Usuario_Listar", commandType: System.Data.CommandType.StoredProcedure).ToList();
            }
        }

        public Usuario BuscarPorUsuario(string usuario)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Usuario>("usp_Usuario_Obtener", new
                {
                    Username = usuario
                }, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public bool Delete(string usuario)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Execute("usp_Usuario_Eliminar", new
                {
                    Username = usuario
                }, commandType: System.Data.CommandType.StoredProcedure);
            }

            return true;
        }



        public Usuario BuscarPorUsuarioYPassword(string usuario, string password)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<Usuario>("usp_Usuario_Login",
                                                 new
                                                 {
                                                     Username = usuario,
                                                     Password = password
                                                 }, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
            }

        }

        public override int Insert(Usuario entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {


                var p = new DynamicParameters();

                p.Add("@Username", entity.Username);
                p.Add("@Password", entity.Password);
                p.Add("@Correo", entity.Correo);
                p.Add("@Nombres", entity.Nombres);
                p.Add("@Apellidos", entity.Apellidos);
                p.Add("@Direccion", entity.Direccion);
                p.Add("@Telefono", entity.Telefono);
                p.Add("@EsAdministrador", entity.EsAdministrador);

                var objeto = connection.ExecuteScalar("usp_Usuario_Insertar",
                    p, commandType: System.Data.CommandType.StoredProcedure);

                return 0;
            }
        }

        public override bool Update(Usuario entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {


                var p = new DynamicParameters();

                p.Add("@Username", entity.Username);
                p.Add("@Password", entity.Password);
                p.Add("@Correo", entity.Correo);
                p.Add("@Nombres", entity.Nombres);
                p.Add("@Apellidos", entity.Apellidos);
                p.Add("@Direccion", entity.Direccion);
                p.Add("@Telefono", entity.Telefono);
                p.Add("@EsAdministrador", entity.EsAdministrador);

                connection.Execute("usp_Usuario_Actualizar", p, commandType: System.Data.CommandType.StoredProcedure);

                return true;
            }
        }


    }
}
