﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zurita.Models;
using Zurita.Repositories.Mascotas;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Data.SqlClient;

namespace Zurita.Repositories.Dapper.Mascotas
{
    public class VisitaHogarRepository: Repository<VisitaHogar>, IVisitaHogarRepository
    {
        public VisitaHogarRepository(string connectionString) : base(connectionString) { }

        public override IEnumerable<VisitaHogar> GetList()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<VisitaHogar>("usp_VisitaHogar_Listar", commandType: System.Data.CommandType.StoredProcedure).ToList();
            }
        }

        public override VisitaHogar GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<VisitaHogar>("usp_VisitaHogar_Obtener", new
                {
                    VisitaHogarId = id
                }, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
            }
        }


        public override bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Execute("usp_VisitaHogar_Eliminar", new
                {
                    VisitaHogarId = id
                }, commandType: System.Data.CommandType.StoredProcedure);
            }

            return true;
        }


        public override int Insert(VisitaHogar entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {


                var p = new DynamicParameters();

                p.Add("@Motivo", !string.IsNullOrWhiteSpace(entity.Motivo) ? entity.Motivo : null);
                p.Add("@Fecha", entity.Fecha);
                p.Add("@Direccion", !string.IsNullOrWhiteSpace(entity.Direccion) ? entity.Direccion : null);
                p.Add("@Observacion", !string.IsNullOrWhiteSpace(entity.Observacion) ? entity.Observacion : null);
                p.Add("@MascotaId", entity.MascotaId ?? null);

                var objeto = connection.ExecuteScalar("usp_VisitaHogar_Insertar",
                    p, commandType: System.Data.CommandType.StoredProcedure);

                return Convert.ToInt32(objeto);
            }
        }

        public override bool Update(VisitaHogar entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var p = new DynamicParameters();

                p.Add("@VisitaHogarId", entity.VisitaHogarId);
                p.Add("@Motivo", !string.IsNullOrWhiteSpace(entity.Motivo) ? entity.Motivo : null);
                p.Add("@Fecha", entity.Fecha);
                p.Add("@Direccion", !string.IsNullOrWhiteSpace(entity.Direccion) ? entity.Direccion : null);
                p.Add("@Observacion", !string.IsNullOrWhiteSpace(entity.Observacion) ? entity.Observacion : null);
                p.Add("@MascotaId", entity.MascotaId ?? null);

                connection.Execute("usp_VisitaHogar_Actualizar", p, commandType: System.Data.CommandType.StoredProcedure);

                return true;
            }
        }
    }
}
