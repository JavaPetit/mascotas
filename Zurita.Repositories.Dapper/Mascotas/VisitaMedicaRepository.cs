﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zurita.Models;
using Zurita.Repositories.Mascotas;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Data.SqlClient;

namespace Zurita.Repositories.Dapper.Mascotas
{
    public class VisitaMedicaRepository: Repository<VisitaMedica>, IVisitaMedicaRepository
    {
        public VisitaMedicaRepository(string connectionString) : base(connectionString) { }

        public override IEnumerable<VisitaMedica> GetList()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<VisitaMedica>("usp_VisitaMedica_Listar", commandType: System.Data.CommandType.StoredProcedure).ToList();
            }
        }

        public override VisitaMedica GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<VisitaMedica>("usp_VisitaMedica_Obtener", new
                {
                    VisitaMedicaId = id
                }, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public override bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Execute("usp_VisitaMedica_Eliminar", new
                {
                    VisitaMedicaId = id
                }, commandType: System.Data.CommandType.StoredProcedure);
            }

            return true;
        }

        public override int Insert(VisitaMedica entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {


                var p = new DynamicParameters();

                p.Add("@Motivo", !string.IsNullOrWhiteSpace(entity.Motivo) ? entity.Motivo : null);
                p.Add("@Medico", !string.IsNullOrWhiteSpace(entity.Medico) ? entity.Medico : null);
                p.Add("@Fecha", entity.Fecha);
                p.Add("@CondicionCorporal", !string.IsNullOrWhiteSpace(entity.CondicionCorporal) ? entity.CondicionCorporal : null);
                p.Add("@Diagnostico", !string.IsNullOrWhiteSpace(entity.Diagnostico) ? entity.Diagnostico : null);
                p.Add("@Tratamiento", !string.IsNullOrWhiteSpace(entity.Tratamiento) ? entity.Tratamiento : null);
                p.Add("@MascotaId", entity.MascotaId ?? null);

                var objeto = connection.ExecuteScalar("usp_VisitaMedica_Insertar",
                    p, commandType: System.Data.CommandType.StoredProcedure);

                return Convert.ToInt32(objeto);
            }
        }

        public override bool Update(VisitaMedica entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var p = new DynamicParameters();

                p.Add("@VisitaMedicaId", entity.VisitaMedicaId);
                p.Add("@Motivo", !string.IsNullOrWhiteSpace(entity.Motivo) ? entity.Motivo : null);
                p.Add("@Medico", !string.IsNullOrWhiteSpace(entity.Medico) ? entity.Medico : null);
                p.Add("@Fecha", entity.Fecha);
                p.Add("@CondicionCorporal", !string.IsNullOrWhiteSpace(entity.CondicionCorporal) ? entity.CondicionCorporal : null);
                p.Add("@Diagnostico", !string.IsNullOrWhiteSpace(entity.Diagnostico) ? entity.Diagnostico : null);
                p.Add("@Tratamiento", !string.IsNullOrWhiteSpace(entity.Tratamiento) ? entity.Tratamiento : null);
                p.Add("@MascotaId", entity.MascotaId ?? null);


                connection.Execute("usp_VisitaMedica_Actualizar", p, commandType: System.Data.CommandType.StoredProcedure);

                return true;
            }
        }
    }
}
