﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zurita.Models;

namespace Zurita.Repositories.Mascotas
{
    public interface IMascotaRepository : IRepository<Mascota>
    {

        IEnumerable<Mascota> GetList(string especie, string genero);
    }
}
