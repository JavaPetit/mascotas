﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zurita.Models;

namespace Zurita.Repositories.Mascotas
{
    public interface ISolicitudAdopcionRepository : IRepository<SolicitudAdopcion>
    {

        IEnumerable<SolicitudAdopcion> GetList(string username);

        SolicitudAdopcion ObtenerPorMascotaYUsuario(int idMascota, string username);
    }
}
