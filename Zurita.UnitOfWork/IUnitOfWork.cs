﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zurita.Repositories.Mascotas;

namespace Zurita.UnitOfWork
{
    public interface IUnitOfWork
    {
        IMascotaRepository Mascotas { get; }

        ISolicitudAdopcionRepository SolicitudesAdopcion { get; }

        IVisitaMedicaRepository VisitasMedicas { get; }

        IVisitaHogarRepository VisitasHogar { get; }

        IUsuarioRepository Usuarios { get; }

    }
}
