﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Zurita.Models;
using Zurita.UnitOfWork;
using Zurita.WebApi.ViewModels.Mascota;

namespace Zurita.WebApi.Controllers
{
    public class BaseController : ApiController
    {
        protected readonly IUnitOfWork _unit;
        protected readonly ILog _log;
        public BaseController(ILog log, IUnitOfWork unit)
        {
            _unit = unit;
            _log = log;
        }

        protected MascotaViewModel ObtenerMascotaDeModelo(Mascota mascota)
        {
            if (mascota == null)
            {
                return null;
            }

            var viewModel = new MascotaViewModel()
            {
                Color = mascota.Color,
                Especie = mascota.Especie,
                EstaAdoptado = mascota.EstaAdoptado,
                Foto = mascota.Foto,
                MascotaId = mascota.MascotaId,
                Nombre = mascota.Nombre,
                Raza = mascota.Raza,
                Sexo = mascota.Sexo,
                Nacimiento = mascota.Nacimiento.HasValue ? mascota.Nacimiento.Value.ToString("yyyy-MM-dd") : null,
                UrlVideoYoutube = mascota.UrlVideoYoutube,
                Descripcion = mascota.Descripcion
            };


            return viewModel;
        }

    }
}