﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Zurita.UnitOfWork;
using Zurita.WebApi.ViewModels.Mascota;

namespace Zurita.WebApi.Controllers
{
    [RoutePrefix("api/mascota")]
    public class MascotaController : BaseController
    {
        public MascotaController(ILog log, IUnitOfWork unit) : base(log, unit)
        {
        }

        [HttpGet()]
        [Route("Listar")]
        public List<MascotaViewModel> ListarMascotas([FromUri]string especie = null, [FromUri]string sexo = null)
        {

            var modelos = _unit.Mascotas.GetList(especie, sexo);
            var lista = new List<MascotaViewModel>();

            foreach (var modelo in modelos)
            {
                lista.Add(ObtenerMascotaDeModelo(modelo));
            }

            return lista;
        }
    }
}