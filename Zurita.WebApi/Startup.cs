﻿using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Zurita.WebApi.App_Start;
using Zurita.WebApi.Handlers;

[assembly: OwinStartup(typeof(Zurita.WebApi.Startup))]
namespace Zurita.WebApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            log4net.Config.XmlConfigurator.Configure();
            var log = log4net.LogManager.GetLogger(typeof(Startup));
            log.Debug("Logging is enabled");

            var config = new HttpConfiguration();
            config.Services.Replace(typeof(IExceptionHandler), new GlobalExceptionHandler());

            DIConfig.ConfigureInjector(config);

            RouteConfig.Register(config);
            WebApiConfig.Configure(config);
            app.UseWebApi(config);

        }
    }
}